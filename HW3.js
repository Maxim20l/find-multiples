"use script";
// 1. Цикли потрібні для того що б облегшити робуту і не писати цикл власноруч, а просто задати дані в while чи for
// 2.  По факту while for схожі за собою і той і той перевіряють роботу перед ітерацією, але в for  можна задати більше умов тому якщо потрібно зробити не складний цикл, то краще while і навпакию
// 3. Явне приведення типів це приведення за допомогою Number(value), а неявне це перетворення, наприклад 2/"4".


let num;
do {
   num = +prompt('pass number', 0);
} while (!Number.isInteger(num)) {
    if (num < 5 || num == "") {
        console.log("Sorry, no numbers");
      }  else if (num) {
    for ( let i = 1; i <= num; i++) {
      if ( i % 5 !== 0) continue;
      console.log(i);
    }
}
} 
